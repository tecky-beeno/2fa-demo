import { Knex } from 'knex'
import { EmailService } from './email.service'
import jwt from 'jwt-simple'
import { env } from './env'

export type JWTPayload = {
  id: number
}

const PASSCODE_EXPIRE_INTERVAL = 1000 * 60 * 5 // 5 minutes
export class UserService {
  // passcode -> login attempt
  private passcode_map = new Map<string, { email: string; timer: any }>()

  // email -> trial
  private email_trial_map = new Map<string, number>()

  constructor(public knex: Knex, public emailService: EmailService) {}

  private async getUserIdByEmail(email: string) {
    let user = await this.knex
      .select('id')
      .from('user')
      .where({ email })
      .first()
    if (user) {
      return user.id as number
    }
    let rows: any[] = await this.knex
      .insert({ email })
      .into('user')
      .returning('id')
    return rows[0].id as number
  }

  public async sendPasscode(email: string) {
    for (;;) {
      let passcode = Math.random().toString().slice(-6)
      if (this.passcode_map.has(passcode)) {
        continue
      }

      let href = `${env.REACT_APP_ORIGIN}/login/passcode/${passcode}`
      await this.emailService.sendEmail({
        email_address: email,
        title: `Login to ${env.APP_NAME}`,
        body: /* html */ `
<h1>Welcome to ${env.APP_NAME}</h1>
<p>You can use the following link to login</p>
<a href="${href}">${href}</a>
`,
      })

      let timer = setTimeout(() => {
        this.passcode_map.delete(passcode)
      }, PASSCODE_EXPIRE_INTERVAL)

      this.passcode_map.set(passcode, { email, timer })
      this.email_trial_map.set(email, 0)

      return
    }
  }

  public async getToken(email: string, passcode: string): Promise<string> {
    let trial = this.email_trial_map.get(email)
    if (trial === undefined) {
      throw new Error('wrong email')
    }
    if (trial > 3) {
      throw new Error('passcode expired, please retry later')
    }
    this.email_trial_map.set(email, trial + 1)
    let pair = this.passcode_map.get(passcode)
    if (!pair) {
      throw new Error('wrong or expired passcode')
    }
    if (email !== pair.email) {
      throw new Error('invalid passcode')
    }
    this.passcode_map.delete(passcode)
    clearTimeout(pair.timer)
    let payload: JWTPayload = {
      id: await this.getUserIdByEmail(email),
    }
    let token: string = jwt.encode(payload, env.JWT_SECRET)
    return token
  }
}
