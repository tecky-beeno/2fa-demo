import { env } from './env'

export class SmsService {
  public async sendMessage(message: { tel: string; content: string }) {
    const url = `https://api.wavecell.com/sms/v1/${env.SMS_ACCOUNT_KEY}/single`
    let res = await fetch(url, {
      method: 'POST',
      cache: 'no-cache',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + env.SMS_API_KEY,
      },
      body: JSON.stringify({
        source: 'FDragon',
        destination: message.tel,
        text: message.content,
      }),
    })
    if (!res.ok) {
      throw new Error(await res.text())
    }
  }
}
