import { Knex } from 'knex'
import { EmailService } from './email.service'
import jwt from 'jwt-simple'
import { env } from './env'
import { SmsService } from './sms.service'

export type JWTPayload = {
  id: number
}

const PASSCODE_EXPIRE_INTERVAL = 1000 * 60 * 5 // 5 minutes
export class UserServiceV2 {
  // passcode -> login attempt
  private passcode_map = new Map<string, { contact: string; timer: any }>()

  // email or tel -> trial
  private contact_trial_map = new Map<string, number>()

  constructor(
    public knex: Knex,
    public emailService: EmailService,
    public smsService: SmsService,
  ) {}

  private async getUserIdByContact(contact: string) {
    let user = contact.includes('@') ? { email: contact } : { tel: contact }
    let row = await this.knex.select('id').from('user').where(user).first()
    if (row) {
      return row.id as number
    }
    let rows: any[] = await this.knex.insert(user).into('user').returning('id')
    return rows[0].id as number
  }

  public async sendPasscodeByEmail(email: string) {
    await this.sendPasscode(async passcode => {
      let href = `${env.REACT_APP_ORIGIN}/login/passcode/${passcode}`
      await this.emailService.sendEmail({
        email_address: email,
        title: `Login to ${env.APP_NAME}`,
        body: /* html */ `
<h1>Welcome to ${env.APP_NAME}</h1>
<p>You can use the following link to login</p>
<a href="${href}">${href}</a>
`,
      })
      return email
    })
  }

  public async sendPasscodeByTel(tel: string) {
    await this.sendPasscode(async passcode => {
      await this.smsService.sendMessage({
        tel,
        content: `${passcode} is your verification code for ${env.APP_NAME}`,
      })
      return tel
    })
  }

  private async sendPasscode(
    sendPasscodeFn: (passcode: string) => Promise<string>,
  ) {
    for (;;) {
      let passcode = Math.random().toString().slice(-6)
      if (this.passcode_map.has(passcode)) {
        continue
      }

      let contact = await sendPasscodeFn(passcode)

      let timer = setTimeout(() => {
        this.passcode_map.delete(passcode)
      }, PASSCODE_EXPIRE_INTERVAL)

      this.passcode_map.set(passcode, { contact: contact, timer })
      this.contact_trial_map.set(contact, 0)

      return
    }
  }

  public async getToken(contact: string, passcode: string): Promise<string> {
    let trial = this.contact_trial_map.get(contact)
    if (trial === undefined) {
      throw new Error('wrong email or tel')
    }
    if (trial > 3) {
      throw new Error('passcode expired, please retry later')
    }
    this.contact_trial_map.set(contact, trial + 1)
    let pair = this.passcode_map.get(passcode)
    if (!pair) {
      throw new Error('wrong or expired passcode')
    }
    if (contact !== pair.contact) {
      throw new Error('invalid passcode')
    }
    this.passcode_map.delete(passcode)
    clearTimeout(pair.timer)
    let payload: JWTPayload = {
      id: await this.getUserIdByContact(contact),
    }
    let token: string = jwt.encode(payload, env.JWT_SECRET)
    return token
  }
}
