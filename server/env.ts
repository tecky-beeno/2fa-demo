import { config } from 'dotenv'
import populateEnv from 'populate-env'

export let env = {
  JWT_SECRET: '',
  REACT_APP_ORIGIN: '',
  APP_NAME: '',
  EMAIL_HOST: '',
  EMAIL_PORT: 0,
  EMAIL_USER: '',
  EMAIL_PASSWORD: '',
  SMS_ACCOUNT_KEY: '',
  SMS_API_KEY: '',
}
config()
populateEnv(env)
