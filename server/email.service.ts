import { createTransport } from 'nodemailer'
import { env } from './env'

const EMAIL_INTERVAL = 1000 * 60 * 3 // 3 minutes

export class EmailService {
  // for oauth login
  // transporter = createTransport({
  //   service: env.EMAIL_SERVICE,
  //   host: env.EMAIL_HOST,
  //   port: env.EMAIL_PORT,
  //   auth: {
  //     clientId: env.EMAIL_CLIENT_ID,
  //     clientSecret: env.EMAIL_CLIENT_SECRET,
  //   },
  // })

  // for password login
  private transporter = createTransport({
    host: env.EMAIL_HOST,
    port: env.EMAIL_PORT,
    auth: {
      user: env.EMAIL_USER,
      pass: env.EMAIL_PASSWORD,
    },
  })

  // to avoid spamming the email too often
  private email_cool_down_set = new Set<string>()

  public async sendEmail(email: {
    email_address: string
    title: string
    body: string
  }) {
    let address = email.email_address
    if (this.email_cool_down_set.has(address)) {
      throw new Error('Sending too frequently, please retry later')
    }
    await this.transporter.sendMail({
      from: env.EMAIL_USER,
      to: email.email_address,
      subject: email.title,
      html: email.body,
    })
    this.email_cool_down_set.add(address)
    setTimeout(() => {
      this.email_cool_down_set.delete(address)
    }, EMAIL_INTERVAL)
  }
}
