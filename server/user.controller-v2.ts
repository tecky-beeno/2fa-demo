import express from 'express'
import { UserServiceV2 } from './user.service-v2'

export class UserControllerV2 {
  router = express.Router()

  constructor(public userService: UserServiceV2) {
    this.router.post('/login/email', this.requestPasscodeWithEmail)
    this.router.post('/login/tel', this.requestPasscodeWithTel)
    this.router.post('/login/passcode', this.requestToken)
  }

  requestPasscodeWithEmail = async (
    req: express.Request,
    res: express.Response,
  ) => {
    try {
      let { email } = req.body
      if (!email) {
        res.status(400).json({ error: 'missing email in req.body' })
        return
      }
      await this.userService.sendPasscodeByEmail(email)
      res.json({ message: 'sent to your email' })
    } catch (error) {
      res.status(500).json({ error: String(error) })
    }
  }

  requestPasscodeWithTel = async (
    req: express.Request,
    res: express.Response,
  ) => {
    try {
      let { tel } = req.body
      if (!tel) {
        res.status(400).json({ error: 'missing email or tel in req.body' })
      }
      await this.userService.sendPasscodeByTel(tel)
      res.json({ message: 'sent to your mailbox' })
    } catch (error) {
      res.status(500).json({ error: String(error) })
    }
  }

  requestToken = async (req: express.Request, res: express.Response) => {
    try {
      let { email, tel, passcode } = req.body
      let contact = email || tel
      if (!contact) {
        res.status(400).json({ error: 'missing email or tel in req.body' })
        return
      }
      if (!passcode) {
        res.status(400).json({ error: 'missing passcode in req.body' })
        return
      }
      let token = await this.userService.getToken(contact, passcode)
      res.json({ token })
    } catch (error) {
      res.status(500).json({ error: String(error) })
    }
  }
}
