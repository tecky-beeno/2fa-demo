import express from 'express'
import { UserService } from './user.service'

export class UserController {
  router = express.Router()

  constructor(public userService: UserService) {
    this.router.post('/login/email', this.requestPasscode)
    this.router.post('/login/passcode', this.requestToken)
  }

  requestPasscode = async (req: express.Request, res: express.Response) => {
    try {
      let { email } = req.body
      if (!email) {
        res.status(400).json({ error: 'missing email in req.body' })
        return
      }
      await this.userService.sendPasscode(email)
      res.json({ message: 'sent to your email' })
    } catch (error) {
      res.status(500).json({ error: String(error) })
    }
  }

  requestToken = async (req: express.Request, res: express.Response) => {
    try {
      let { email, passcode } = req.body
      if (!email) {
        res.status(400).json({ error: 'missing email in req.body' })
        return
      }
      if (!passcode) {
        res.status(400).json({ error: 'missing passcode in req.body' })
        return
      }
      let token = await this.userService.getToken(email, passcode)
      res.json({ token })
    } catch (error) {
      res.status(500).json({ error: String(error) })
    }
  }
}
