import React, { useEffect, useState } from 'react'
import './App.css'
import { useParams, Router, Route, Link } from 'react-router-dom'
import { config } from './env'
import { createBrowserHistory } from 'history'

const history = createBrowserHistory()

async function post(url: string, body: object) {
  try {
    let res = await fetch(config.API_ORIGIN + url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body),
    })
    let json = await res.json()
    return json
  } catch (error) {
    return { error: String(error) }
  }
}

function LoginPage() {
  const [email, setEmail] = useState('')
  const [message, setMessage] = useState('')
  async function requestPasscode() {
    localStorage.setItem('email', email)
    setMessage('')
    let json = await post('/login/email', { email })
    setMessage(
      json.error ||
        "We've just sent an email to you. Please check your inbox. (Just in case, also check your spam folder)",
    )
  }
  return (
    <div>
      <input
        type="email"
        value={email}
        onChange={e => setEmail(e.currentTarget.value)}
      />
      <button onClick={requestPasscode}>Request Passcode</button>
      {message ? <p>{message}</p> : null}
    </div>
  )
}

function VerifyPasscodePage() {
  let params = useParams<{ passcode: string }>()
  let passcode = params.passcode
  let email = localStorage.getItem('email')
  const [error, setError] = useState('')
  useEffect(() => {
    if (!passcode) {
      setError('missing passcode')
      return
    }
    if (!email) {
      setError('missing email')
      return
    }
    setError('')
    post('/login/passcode', { passcode, email }).then(json =>
      setError(json.error),
    )
  }, [passcode, email])
  return (
    <div>
      {error ? (
        <>
          <p style={{ color: 'red' }}>{error}</p>
          <Link to="/login">Try again</Link>
        </>
      ) : null}
    </div>
  )
}

function App() {
  return (
    <div className="App">
      <Router history={history}>
        <Route
          path="/login/passcode/:passcode"
          component={VerifyPasscodePage}
        />
        <Route path="/login" component={LoginPage} />
      </Router>
    </div>
  )
}

export default App
